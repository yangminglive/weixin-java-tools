package me.chanjar.weixin.cp.bean.scan;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class ScanAgentInfo {
	private int agentid;
	@SerializedName("auth_type")
	private int authType;
}
