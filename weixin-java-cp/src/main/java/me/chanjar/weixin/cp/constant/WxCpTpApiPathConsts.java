package me.chanjar.weixin.cp.constant;

public class WxCpTpApiPathConsts {

	public static class OAuth2 {
		public static final String GET_USER_INFO = "/cgi-bin/service/getuserinfo3rd?code=%s&suite_access_token=%s";
		public static final String GET_USER_DETAIL = "/cgi-bin/service/getuserdetail3rd?suite_access_token=%s";
		public static final String URL_OAUTH2_AUTHORIZE = me.chanjar.weixin.cp.constant.WxCpApiPathConsts.OAuth2.URL_OAUTH2_AUTHORIZE;
	}
	
	public static class ScanQr {
		public static final String GET_USER_INFO = "/cgi-bin/service/get_login_info?suite_access_token=%s";
		public static final String URL_SCAN_QR_AUTHORIZE = "https://open.work.weixin.qq.com/wwopen/sso/3rd_qrConnect";
	}
}
