package me.chanjar.weixin.cp.api.impl;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.util.http.URIUtil;
import me.chanjar.weixin.common.util.json.GsonHelper;
import me.chanjar.weixin.cp.api.WxCpOAuth2Service;
import me.chanjar.weixin.cp.api.WxCpService;
import me.chanjar.weixin.cp.api.WxCpTpOAuth2Service;
import me.chanjar.weixin.cp.api.WxCpTpScanQrService;
import me.chanjar.weixin.cp.api.WxCpTpService;
import me.chanjar.weixin.cp.bean.WxCpOauth2UserInfo;
import me.chanjar.weixin.cp.bean.WxCpUserDetail;
import me.chanjar.weixin.cp.bean.scan.WxScanCpTpUserInfo;
import me.chanjar.weixin.cp.constant.WxCpApiPathConsts;
import me.chanjar.weixin.cp.util.json.WxCpGsonBuilder;

import static me.chanjar.weixin.common.api.WxConsts.OAuth2Scope.*;
import static me.chanjar.weixin.common.api.WxConsts.OAuth2Scope.SNSAPI_PRIVATEINFO;
import static me.chanjar.weixin.common.api.WxConsts.OAuth2Scope.SNSAPI_USERINFO;
import static me.chanjar.weixin.cp.constant.WxCpTpApiPathConsts.ScanQr.*;

/**
 * <pre>
 * 扫码相关接口实现类.
 * Created by Binary Wang on 2017-6-25.
 * </pre>
 *
 * @author <a href="https://github.com/binarywang">Binary Wang</a>
 */
@RequiredArgsConstructor
public class WxCpTpScanQrServiceImpl implements WxCpTpScanQrService {
	private final WxCpTpService mainTpService;//本公司配置
	
	@Override
	public String buildAuthorizationUrl(String redirectUri,String state) {
		StringBuilder url = new StringBuilder(URL_SCAN_QR_AUTHORIZE);//appid=ww100000a5f2191&redirect_uri=http%3A%2F%2Fwww.oa.com&state=web_login@gyoss9&usertype=admin
		url.append("?appid=").append(this.mainTpService.getWxCpTpConfigStorage().getCorpId());
		url.append("&redirect_uri=").append(URIUtil.encodeURIComponent(redirectUri));
		url.append("&usertype=member&state=").append(state);
		
		return url.toString();
	}

	@Override
	public WxScanCpTpUserInfo getUserInfo(String code) throws WxErrorException {
		String providerAccessToken = this.mainTpService.getProviderToken();
		String responseText = this.mainTpService.get(String.format(this.mainTpService.getWxCpTpConfigStorage().getApiUrl(GET_USER_INFO), code, providerAccessToken), null);
		
		return WxCpGsonBuilder.create().fromJson(responseText, WxScanCpTpUserInfo.class);
	}
}
