package me.chanjar.weixin.cp.api;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.cp.bean.scan.WxScanCpTpUserInfo;

/**
 * <pre>
 * 扫码相关管理接口.
 *  Created by BinaryWang on 2017/6/24.
 * </pre>
 *
 * @author <a href="https://github.com/binarywang">Binary Wang</a>
 */
public interface WxCpTpScanQrService{

	  /**
	   * <pre>
	   * 构造扫码授权的url连接
	   * 详情请见: http://qydev.weixin.qq.com/wiki/index.php?title=企业获取code
	   * </pre>
	   *
	   * @param redirectUri 跳转链接地址
	   * @param state       状态码
	   * @param scope       取值参考me.chanjar.weixin.common.api.WxConsts.OAuth2Scope类
	   * @return url
	   */
	  String buildAuthorizationUrl(String redirectUri,String state);

	  /**
	   * <pre>
	   * 根据code获取成员信息
	   * http://qydev.weixin.qq.com/wiki/index.php?title=根据code获取成员信息
	   * https://work.weixin.qq.com/api/doc#10028/根据code获取成员信息
	   * https://work.weixin.qq.com/api/doc#90000/90135/91023  获取访问用户身份
	   * 因为企业号oauth2.0必须在应用设置里设置通过ICP备案的可信域名，所以无法测试，因此这个方法很可能是坏的。
	   *
	   * 注意: 这个方法不使用WxCpConfigStorage里的agentId，需要开发人员自己给出
	   * </pre>
	   *
	   * @param providerAccessToken 服务商的token:以corpid、provider_secret（获取方法为：登录服务商管理后台->标准应用服务->通用开发参数，可以看到）换取provider_access_token，代表的是服务商的身份，而与应用无关。请求单点登录、注册定制化等接口需要用到该凭证。
	   * @param code    通过成员授权获取到的code，最大为512字节。每次成员授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期。
	   * @return WxScanCpTpUserInfo
	   * @throws WxErrorException 异常
	   * @see #getUserInfo(String)
	   */
	  WxScanCpTpUserInfo getUserInfo(String code) throws WxErrorException;
}
