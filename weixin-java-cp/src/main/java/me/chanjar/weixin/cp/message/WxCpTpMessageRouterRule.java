package me.chanjar.weixin.cp.message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import me.chanjar.weixin.common.api.WxErrorExceptionHandler;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.cp.api.WxCpTpService;
import me.chanjar.weixin.cp.bean.WxCpTpXmlMessage;

public class WxCpTpMessageRouterRule {

  private final WxCpTpMessageRouter routerBuilder;

  private boolean async = true;

  private String changeType;
  
  private String infoType;

  private String infoTypeRegex;

  private WxCpTpMessageMatcher matcher;

  private boolean reEnter = false;

  private List<WxCpTpMessageHandler> handlers = new ArrayList<>();

  private List<WxCpTpMessageInterceptor> interceptors = new ArrayList<>();

  protected WxCpTpMessageRouterRule(WxCpTpMessageRouter routerBuilder) {
    this.routerBuilder = routerBuilder;
  }

  /**
   * 设置是否异步执行，默认是true
   *
   * @param async
   */
  public WxCpTpMessageRouterRule async(boolean async) {
    this.async = async;
    return this;
  }

  /**
   * 如果infoType等于某值
   *
   * @param eventKey
   */
  public WxCpTpMessageRouterRule infoType(String infoType) {
    this.infoType = infoType;
    return this;
  }
  /**
   * 如果changeType等于某值
   *
   * @param eventKey
   */
  public WxCpTpMessageRouterRule changeType(String changeType) {
    this.changeType = changeType;
    return this;
  }

  /**
   * 如果infoType匹配该正则表达式
   */
  public WxCpTpMessageRouterRule infoTypeRegex(String regex) {
    this.infoTypeRegex = regex;
    return this;
  }

  /**
   * 如果消息匹配某个matcher，用在用户需要自定义更复杂的匹配规则的时候
   *
   * @param matcher
   */
  public WxCpTpMessageRouterRule matcher(WxCpTpMessageMatcher matcher) {
    this.matcher = matcher;
    return this;
  }

  /**
   * 设置微信消息拦截器
   *
   * @param interceptor
   */
  public WxCpTpMessageRouterRule interceptor(WxCpTpMessageInterceptor interceptor) {
    return interceptor(interceptor, (WxCpTpMessageInterceptor[]) null);
  }

  /**
   * 设置微信消息拦截器
   *
   * @param interceptor
   * @param otherInterceptors
   */
  public WxCpTpMessageRouterRule interceptor(WxCpTpMessageInterceptor interceptor, WxCpTpMessageInterceptor... otherInterceptors) {
    this.interceptors.add(interceptor);
    if (otherInterceptors != null && otherInterceptors.length > 0) {
      for (WxCpTpMessageInterceptor i : otherInterceptors) {
        this.interceptors.add(i);
      }
    }
    return this;
  }

  /**
   * 设置微信消息处理器
   *
   * @param handler
   */
  public WxCpTpMessageRouterRule handler(WxCpTpMessageHandler handler) {
    return handler(handler, (WxCpTpMessageHandler[]) null);
  }

  /**
   * 设置微信消息处理器
   *
   * @param handler
   * @param otherHandlers
   */
  public WxCpTpMessageRouterRule handler(WxCpTpMessageHandler handler, WxCpTpMessageHandler... otherHandlers) {
    this.handlers.add(handler);
    if (otherHandlers != null && otherHandlers.length > 0) {
      for (WxCpTpMessageHandler i : otherHandlers) {
        this.handlers.add(i);
      }
    }
    return this;
  }

  /**
   * 规则结束，代表如果一个消息匹配该规则，那么它将不再会进入其他规则
   */
  public WxCpTpMessageRouter end() {
    this.routerBuilder.getRules().add(this);
    return this.routerBuilder;
  }

  /**
   * 规则结束，但是消息还会进入其他规则
   */
  public WxCpTpMessageRouter next() {
    this.reEnter = true;
    return end();
  }

  protected boolean test(WxCpTpXmlMessage wxMessage) {
    return
		(this.changeType == null || this.changeType.equalsIgnoreCase(wxMessage.getChangeType()))
        &&
        (this.infoType == null || this.infoType.equalsIgnoreCase(wxMessage.getInfoType()))
        &&
        (this.infoTypeRegex == null || Pattern.matches(this.infoTypeRegex, StringUtils.trimToEmpty(wxMessage.getInfoType())))
        &&
        (this.matcher == null || this.matcher.match(wxMessage))
      ;
  }

  /**
   * 处理微信推送过来的消息
   *
   * @param wxMessage
   * @return true 代表继续执行别的router，false 代表停止执行别的router
   */
  protected String service(WxCpTpXmlMessage wxMessage,
                                      Map<String, Object> context,
                                      WxCpTpService wxCpTpService,
                                      WxSessionManager sessionManager,
                                      WxErrorExceptionHandler exceptionHandler) {

    if (context == null) {
      context = new HashMap<>();
    }

    try {
      // 如果拦截器不通过
      for (WxCpTpMessageInterceptor interceptor : this.interceptors) {
        if (!interceptor.intercept(wxMessage, context, wxCpTpService, sessionManager)) {
          return null;
        }
      }

      // 交给handler处理
      String res = null;
      for (WxCpTpMessageHandler handler : this.handlers) {
        // 返回最后handler的结果
        res = handler.handle(wxMessage, context, wxCpTpService, sessionManager);
      }
      return res;

    } catch (WxErrorException e) {
      exceptionHandler.handle(e);
    }

    return null;

  }

  public void setMatcher(WxCpTpMessageMatcher matcher) {
    this.matcher = matcher;
  }


  public void setHandlers(List<WxCpTpMessageHandler> handlers) {
    this.handlers = handlers;
  }

  public void setInterceptors(List<WxCpTpMessageInterceptor> interceptors) {
    this.interceptors = interceptors;
  }

  public boolean isAsync() {
    return this.async;
  }

  public void setAsync(boolean async) {
    this.async = async;
  }

  public boolean isReEnter() {
    return this.reEnter;
  }

  public void setReEnter(boolean reEnter) {
    this.reEnter = reEnter;
  }

}
