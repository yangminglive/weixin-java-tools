package me.chanjar.weixin.cp.bean.scan;

import java.util.List;

import lombok.Data;
@Data
public class ScanAuthInfo {
	private List<ScanDepartmentInfo>department;
}
