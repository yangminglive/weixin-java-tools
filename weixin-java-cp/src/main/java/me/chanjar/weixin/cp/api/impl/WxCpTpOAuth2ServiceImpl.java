package me.chanjar.weixin.cp.api.impl;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.util.http.URIUtil;
import me.chanjar.weixin.common.util.json.GsonHelper;
import me.chanjar.weixin.cp.api.WxCpOAuth2Service;
import me.chanjar.weixin.cp.api.WxCpService;
import me.chanjar.weixin.cp.api.WxCpTpOAuth2Service;
import me.chanjar.weixin.cp.api.WxCpTpService;
import me.chanjar.weixin.cp.bean.WxCpOauth2UserInfo;
import me.chanjar.weixin.cp.bean.WxCpUserDetail;
import me.chanjar.weixin.cp.constant.WxCpApiPathConsts;
import me.chanjar.weixin.cp.util.json.WxCpGsonBuilder;

import static me.chanjar.weixin.common.api.WxConsts.OAuth2Scope.*;
import static me.chanjar.weixin.common.api.WxConsts.OAuth2Scope.SNSAPI_PRIVATEINFO;
import static me.chanjar.weixin.common.api.WxConsts.OAuth2Scope.SNSAPI_USERINFO;
import static me.chanjar.weixin.cp.constant.WxCpTpApiPathConsts.OAuth2.*;

/**
 * <pre>
 * oauth2相关接口实现类.
 * Created by Binary Wang on 2017-6-25.
 * </pre>
 *
 * @author <a href="https://github.com/binarywang">Binary Wang</a>
 */
@RequiredArgsConstructor
public class WxCpTpOAuth2ServiceImpl implements WxCpTpOAuth2Service {
	private final WxCpTpService mainTpService;
	@Override
	public String buildAuthorizationUrl(String state) {
		return this.buildAuthorizationUrl(this.mainTpService.getWxCpTpConfigStorage().getOauth2redirectUri(), state);
	}

	@Override
	public String buildAuthorizationUrl(String redirectUri, String state) {
		return this.buildAuthorizationUrl(redirectUri, state, SNSAPI_BASE);
	}

	@Override
	public String buildAuthorizationUrl(String redirectUri, String state, String scope) {
		StringBuilder url = new StringBuilder(URL_OAUTH2_AUTHORIZE);
		url.append("?appid=").append(this.mainTpService.getWxCpTpConfigStorage().getSuiteId());
		url.append("&redirect_uri=").append(URIUtil.encodeURIComponent(redirectUri));
		url.append("&response_type=code");
		url.append("&scope=").append(scope);
		if (state != null) {
			url.append("&state=").append(state);
		}
		url.append("#wechat_redirect");
		return url.toString();
	}

	@Override
	public WxCpOauth2UserInfo getUserInfo(String code) throws WxErrorException {
		return this.getUserInfo(this.mainTpService.getWxCpTpConfigStorage().getSuiteAccessToken(), code);
	}

	@Override
	public WxCpUserDetail getUserDetail(String userTicket) throws WxErrorException {
		JsonObject param = new JsonObject();
		param.addProperty("user_ticket", userTicket);
		String url = String.format(this.mainTpService.getWxCpTpConfigStorage().getApiUrl(GET_USER_DETAIL), this.mainTpService.getWxCpTpConfigStorage().getSuiteAccessToken());
		String responseText = this.mainTpService.post(url,
				param.toString());
		return WxCpGsonBuilder.create().fromJson(responseText, WxCpUserDetail.class);
	}

	@Override
	public WxCpOauth2UserInfo getUserInfo(String suiteAccessToken, String code) throws WxErrorException {
		String responseText = this.mainTpService.get(String.format(this.mainTpService.getWxCpTpConfigStorage().getApiUrl(GET_USER_INFO), code, this.mainTpService.getWxCpTpConfigStorage().getSuiteAccessToken()), null);
	    JsonElement je = new JsonParser().parse(responseText);
	    JsonObject jo = je.getAsJsonObject();
	    
	    return WxCpOauth2UserInfo.builder()
	    .errcode(GsonHelper.getInteger(jo, "errcode"))
	    .errmsg(GsonHelper.getString(jo, "errmsg"))
	      .userId(GsonHelper.getString(jo, "UserId"))
	      .deviceId(GsonHelper.getString(jo, "DeviceId"))
	      .openUserId(GsonHelper.getString(jo, "open_userid"))
	      .userTicket(GsonHelper.getString(jo, "user_ticket"))
	      .expiresIn(GsonHelper.getString(jo, "expires_in"))
	      .corpId(GsonHelper.getString(jo, "CorpId"))
	      .build();
	}
}
