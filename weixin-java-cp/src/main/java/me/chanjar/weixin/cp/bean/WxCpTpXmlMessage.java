package me.chanjar.weixin.cp.bean;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.util.XmlUtils;
import me.chanjar.weixin.common.util.xml.XStreamCDataConverter;
import me.chanjar.weixin.cp.config.WxCpTpConfigStorage;
import me.chanjar.weixin.cp.util.crypto.WxCpTpCryptUtil;
import me.chanjar.weixin.cp.util.xml.XStreamTransformer;

/**
 * 回调推送的message https://work.weixin.qq.com/api/doc#90001/90143/90612
 *
 * @author zhenjun cai
 */
@XStreamAlias("xml")
@Slf4j
@Data
@EqualsAndHashCode(callSuper = true)
public class WxCpTpXmlMessage extends BaseWxXmlMessage implements Serializable {

	private static final long serialVersionUID = 6031833682211475786L;
	/**
	 * 使用dom4j解析的存放所有xml属性和值的map.
	 */
	private Map<String, Object> allFieldsMap;

	@XStreamAlias("SuiteId")
	@XStreamConverter(value = XStreamCDataConverter.class)
	protected String suiteId;

	@XStreamAlias("InfoType")
	@XStreamConverter(value = XStreamCDataConverter.class)
	protected String infoType;

	@XStreamAlias("AuthCorpId")
	@XStreamConverter(value = XStreamCDataConverter.class)
	protected String authCorpId;

	@XStreamAlias("TimeStamp")
	@XStreamConverter(value = XStreamCDataConverter.class)
	protected String timeStamp;

	@XStreamAlias("SuiteTicket")
	@XStreamConverter(value = XStreamCDataConverter.class)
	protected String suiteTicket;

	@XStreamAlias("AuthCode")
	@XStreamConverter(value = XStreamCDataConverter.class)
	protected String authCode;

	public static WxCpTpXmlMessage fromXml(String xml) {
		// 修改微信变态的消息内容格式，方便解析
		// xml = xml.replace("</PicList><PicList>", "");
		final WxCpTpXmlMessage xmlPackage = XStreamTransformer.fromXml(WxCpTpXmlMessage.class, xml);
		xmlPackage.setAllFieldsMap(XmlUtils.xml2Map(xml));
		return xmlPackage;
	}

	/**
	 * 从加密字符串转换.
	 */
	public static WxCpTpXmlMessage fromEncryptedXml(String encryptedXml, WxCpTpConfigStorage wxCpConfigStorage,
			String timestamp, String nonce, String msgSignature) {
		WxCpTpCryptUtil cryptUtil = new WxCpTpCryptUtil(wxCpConfigStorage);
		String plainText = cryptUtil.decrypt(msgSignature, timestamp, nonce, encryptedXml);
		log.debug("解密后的原始xml消息内容：{}", plainText);
		return fromXml(plainText);
	}

	public static WxCpTpXmlMessage fromEncryptedXml(InputStream is, WxCpTpConfigStorage wxCpConfigStorage, String timestamp,
			String nonce, String msgSignature) {
		try {
			return fromEncryptedXml(IOUtils.toString(is, StandardCharsets.UTF_8), wxCpConfigStorage, timestamp, nonce,
					msgSignature);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
