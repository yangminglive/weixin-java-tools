package me.chanjar.weixin.cp.message;

import me.chanjar.weixin.cp.bean.WxCpTpXmlMessage;

/**
 * 消息匹配器，用在消息路由的时候
 */
public interface WxCpTpMessageMatcher {

  /**
   * 消息是否匹配某种模式
   */
  boolean match(WxCpTpXmlMessage message);

}
