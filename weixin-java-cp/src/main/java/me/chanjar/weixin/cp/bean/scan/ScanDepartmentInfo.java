package me.chanjar.weixin.cp.bean.scan;

import lombok.Data;

@Data
public class ScanDepartmentInfo {
	private int id;
	private boolean writable;
	
}
