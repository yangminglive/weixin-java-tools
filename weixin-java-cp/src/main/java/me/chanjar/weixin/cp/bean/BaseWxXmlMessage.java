package me.chanjar.weixin.cp.bean;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

import lombok.Data;
import me.chanjar.weixin.common.util.xml.IntegerArrayConverter;
import me.chanjar.weixin.common.util.xml.LongArrayConverter;
import me.chanjar.weixin.common.util.xml.XStreamCDataConverter;
import me.chanjar.weixin.cp.bean.WxCpXmlMessage.ExtAttr;

@Data
public class BaseWxXmlMessage {

	///////////////////////
	// 以下都是微信推送过来的消息的xml的element所对应的属性
	///////////////////////
	@XStreamAlias("AgentID")
	private Integer agentId;

	@XStreamAlias("ToUserName")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String toUserName;

	@XStreamAlias("CreateTime")
	private Long createTime;

	/**
	 * 通讯录变更事件. 请参考常量 me.chanjar.weixin.cp.constant.WxCpConsts.ContactChangeType
	 */
	@XStreamAlias("ChangeType")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String changeType;

	/**
	 * 变更信息的成员UserID.
	 */
	@XStreamAlias("UserID")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String userId;

	/**
	 * 变更信息的外部联系人的userid，注意不是企业成员的帐号.
	 */
	@XStreamAlias("ExternalUserID")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String externalUserId;

	/**
	 * 添加此用户的「联系我」方式配置的state参数，可用于识别添加此用户的渠道.
	 */
	@XStreamAlias("State")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String state;

	/**
	 * 欢迎语code，可用于发送欢迎语.
	 */
	@XStreamAlias("WelcomeCode")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String welcomeCode;
	/**
	 * 新的UserID，变更时推送（userid由系统生成时可更改一次）.
	 */
	@XStreamAlias("NewUserID")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String newUserId;

	/**
	 * 成员名称. 或者部门名称
	 */
	@XStreamAlias("Name")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String name;

	/**
	 * 成员部门列表，变更时推送，仅返回该应用有查看权限的部门id.
	 */
	@XStreamAlias("Department")
	@XStreamConverter(value = LongArrayConverter.class)
	private Long[] departments;

	/**
	 * 手机号码.
	 */
	@XStreamAlias("Mobile")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String mobile;

	/**
	 * 职位信息。长度为0~64个字节.
	 */
	@XStreamAlias("Position")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String position;

	/**
	 * 性别，1表示男性，2表示女性.
	 */
	@XStreamAlias("Gender")
	private Integer gender;

	/**
	 * 邮箱.
	 */
	@XStreamAlias("Email")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String email;

	/**
	 * 头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可.
	 */
	@XStreamAlias("Avatar")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String avatar;

	/**
	 * 英文名.
	 */
	@XStreamAlias("EnglishName")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String englishName;

	/**
	 * 上级字段，标识是否为上级。0表示普通成员，1表示上级.
	 */
	@XStreamAlias("IsLeader")
	private Integer isLeader;

	/**
	 * 表示所在部门是否为上级，0-否，1-是，顺序与Department字段的部门逐一对应.
	 */
	@XStreamAlias("IsLeaderInDept")
	@XStreamConverter(value = IntegerArrayConverter.class)
	private Integer[] isLeaderInDept;

	/**
	 * 座机.
	 */
	@XStreamAlias("Telephone")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String telephone;

	/**
	 * 地址.
	 */
	@XStreamAlias("Address")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String address;

	/**
	 * 扩展属性.
	 */
	@XStreamAlias("ExtAttr")
	private ExtAttr extAttrs = new ExtAttr();

	/**
	 * 部门Id.
	 */
	@XStreamAlias("Id")
	private Long id;

	/**
	 * 父部门id.
	 */
	@XStreamAlias("ParentId")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String parentId;

	/**
	 * 部门排序.
	 */
	@XStreamAlias("Order")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String order;

	/**
	 * 标签Id.
	 */
	@XStreamAlias("TagId")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String tagId;

	/**
	 * 标签中新增的成员userid列表，用逗号分隔.
	 */
	@XStreamAlias("AddUserItems")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String addUserItems;

	/**
	 * 标签中删除的成员userid列表，用逗号分隔.
	 */
	@XStreamAlias("DelUserItems")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String delUserItems;

	/**
	 * 标签中新增的部门id列表，用逗号分隔.
	 */
	@XStreamAlias("AddPartyItems")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String addPartyItems;

	/**
	 * 标签中删除的部门id列表，用逗号分隔.
	 */
	@XStreamAlias("DelPartyItems")
	@XStreamConverter(value = XStreamCDataConverter.class)
	private String delPartyItems;
}
