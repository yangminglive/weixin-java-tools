package me.chanjar.weixin.cp.bean.scan;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import me.chanjar.weixin.cp.bean.ResultBase;
/**
 * 扫描授权的消息
 * @description: 
 * @author: yeoman
 * @date: 2020年4月17日
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxScanCpTpUserInfo extends ResultBase{
	/*
	 * errcode	返回码。
		若返回该字段，并且errcode!=0，为调用失败；
		无此errcode字段，或者errcode=0，为调用成功
		errmsg	对返回码的文本描述内容。仅当errcode字段返回时，同时返回errmsg
		usertype	登录用户的类型：1.创建者 2.内部系统管理员 3.外部系统管理员 4.分级管理员 5.成员
		user_info	登录用户的信息
		userid	登录用户的userid，登录用户在通讯录中时返回
		name	登录用户的名字，登录用户在通讯录中时返回，此字段从2019年12月30日起，对新创建服务商不再返回，2020年6月30日起，对所有历史服务商不再返回，第三方页面需要通过通讯录展示组件来展示名字
		avatar	登录用户的头像，登录用户在通讯录中时返回
		corp_info	授权方企业信息
		corpid	授权方企业id
		agent	该管理员在该提供商中能使用的应用列表，当登录用户为管理员时返回
		agentid	应用id
		auth_type	该管理员对应用的权限：1.管理权限，0.使用权限
		auth_info	该管理员拥有的通讯录权限，当登录用户为管理员时返回
	 */
	private Byte usertype;///登录用户的类型：1.创建者 2.内部系统管理员 3.外部系统管理员 4.分级管理员 5.成员
	@SerializedName("user_info")
	private ScanUserInfo userInfo;
	@SerializedName("corp_info")
	private ScanCorpInfo corpInfo;
	private List<ScanAgentInfo> agent;
	@SerializedName("auth_info")
	private ScanAuthInfo authInfo;
}
